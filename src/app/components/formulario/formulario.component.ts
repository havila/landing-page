import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.scss']
})
export class FormularioComponent implements OnInit {

  razas: string[] = [
    'Akita', 'Akita americano', 'Alaskan Husky', 'Bull terrier', 'Bulldog americano', 'Drever',
    'Dunker', 'Haldenstoever', 'Harrier', 'Hokkaido', 'Hovawart'
  ];
  constructor() {}

  ngOnInit() {
  }
}
