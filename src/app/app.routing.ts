import {ModuleWithProviders} from '@angular/core';
import { Routes, RouterModule} from '@angular/router';

import {BlogComponent} from './components/blog/blog.component';
import { FormularioComponent } from './components/formulario/formulario.component';

const appRoutes: Routes = [
    {path: '', component: BlogComponent},
    {path: 'blog', component: BlogComponent},
    {path: 'formulario', component: FormularioComponent},
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
